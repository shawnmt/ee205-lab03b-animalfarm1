///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Shawn Tamashiro <shawnmt@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   4 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()

char* colorName (enum Color color) {

   //Map the enum Color to a string
   switch(color){

      case 0:
         return "Black";
         break;

      case 1:
         return "White";
         break;

      case 2:
         return "Red";
         break;

      case 3:
         return "Blue";
         break;

      case 4:
         return "Green";
         break;

      case 5:
         return "Pink";
         break;

      default:
         return NULL;   //we should never get here
         break;
   }
};

/// Decode the enum Gender into strings for printf()

char* genderName (enum Gender gender) {

   // Map the enum Gender to a string
   switch(gender) {

      case 0:
         return "Male";
         break;

      case 1:
         return "Female";
         break;

      default:
         return NULL;
         break;
   }
};

/// Decode the enum CatBreeds into strings for printf()

char* breedName  (enum CatBreeds breeds) {

   // MAp the enum CatBreeds to a string
   switch(breeds) {

      case 0:
         return "Main Coon";
         break;

      case 1:
         return "Manx";
         break;

      case 2:
         return "Shorthair";
         break;

      case 3:
         return "Persian";
         break;

      case 4:
         return "Sphinx";
         break;

      default:
         return NULL;
         break;
   }
};

/// Decode the bool isFixed into strings for printf()

char* fixed (bool isFixed) {

   if(true)
      return "Yes";
   else if(false)
      return "No";
   else
      return NULL;

};




//   return NULL; // We should never get here
// };



